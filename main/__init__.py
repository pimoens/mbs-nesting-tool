from kivy.uix.screenmanager import Screen, SlideTransition


class Main(Screen):

    def disconnect(self):
        self.manager.transition = SlideTransition(direction="up")
        self.manager.current = 'login'
