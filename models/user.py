import hashlib


class User:

    def __init__(self, username, password, roles: list):
        self._username = username
        self._password = self._hash_password(password)
        self._roles = roles

    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, username):
        self._username = username

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = self._hash_password(password)

    @property
    def roles(self):
        return self._roles

    @roles.setter
    def roles(self, roles):
        self._roles = roles

    def has_role(self,  role):
        for _role in self._roles:
            if _role.name == role:
                return True

    @staticmethod
    def _hash_password(password):
        m = hashlib.sha512(password.encode('UTF-8'))
        return m.hexdigest()


if __name__ == '__main__':
    user = User('test', 'test', [])
    print(user.password, len(user.password))
    print(user.username)
