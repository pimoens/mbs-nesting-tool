from kivy.app import App
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import ScreenManager

from login import Login
from main import Main


class NestingTool(App):
    username = StringProperty(None)

    def build(self):
        manager = ScreenManager()

        manager.add_widget(Login(name='login'))
        manager.add_widget(Main(name='main'))

        return manager


if __name__ == '__main__':
    NestingTool().run()
