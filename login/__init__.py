from kivy.app import App
from kivy.uix.screenmanager import Screen, SlideTransition


class Login(Screen):

    def login(self, username, password):
        app = App.get_running_app()

        user = self._validate_credentials(username, password)
        if user is not None:
            app.current_user = user

            self.manager.transition = SlideTransition(direction="down")
            self.manager.current = 'main'
        else:
            self.ids.error.text = 'Invalid username/password combination!'

    def on_pre_enter(self, *args):
        self.ids.error.text = ''
        self.ids.username.text = ''
        self.ids.password.text = ''

    @staticmethod
    def _validate_credentials(username, password):
        return True
